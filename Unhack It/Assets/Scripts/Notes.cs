﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notes : MonoBehaviour {
    public string notes;
    public InputField notesIF;

    // Use this for initialization
    void Start() {
        if (PlayerPrefs.HasKey("Notes") == true)
        {
            notes = PlayerPrefs.GetString("Notes");
        }
        notesIF.text = notes;
        StartCoroutine(NotesAutoSave());
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
#pragma warning disable CS0618 // Type or member is obsolete
            Application.LoadLevel(1);
#pragma warning restore CS0618 // Type or member is obsolete
    }
    IEnumerator NotesAutoSave()
    {
        notes = notesIF.text;
        
        yield return new WaitForSeconds(0.5f);
        PlayerPrefs.SetString("Notes", notes);
        PlayerPrefs.Save();
        
        StartCoroutine(NotesAutoSave());
    }
}
