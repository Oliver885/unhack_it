﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject doorNormal;
    public GameObject openedDoor;
    public GameObject closedDoor;
    public GameObject compOpenPanel;
    public GameObject player;

	public bool doorOpened = false;
	public bool collisionPC = false;

    public float playerPosX;
    public float playerPosY;
    public float playerPosZ;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
		if (collisionPC == true)
		{
		if (Input.GetKeyDown(KeyCode.E))
		{
#pragma warning disable CS0618 // Type or member is obsolete
                Application.LoadLevel(1);
#pragma warning restore CS0618 // Type or member is obsolete
            }
            playerPosX = player.transform.position.x;
            PlayerPrefs.SetFloat("PlayerX", playerPosX);
            PlayerPrefs.Save();
            playerPosY = player.transform.position.y;
            PlayerPrefs.SetFloat("PlayerY", playerPosY);
            PlayerPrefs.Save();
            playerPosZ = player.transform.position.z;
            PlayerPrefs.SetFloat("PlayerZ", playerPosZ);
            PlayerPrefs.Save();
		}
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Door") == true)
        {
			doorNormal.transform.rotation = Quaternion.Euler(openedDoor.transform.rotation.eulerAngles);
			StartCoroutine (ClosedDoor ());
        }

        if (other.gameObject.CompareTag("PC table") == true)
        {
            compOpenPanel.SetActive(true);
			collisionPC = true;

        }

    }
	IEnumerator ClosedDoor()
	{
		yield return new WaitForSeconds(2.0f);
		doorNormal.transform.rotation = Quaternion.Euler (closedDoor.transform.rotation.eulerAngles);
	}
	void OnCollisionExit(Collision other)
	{
		if (other.gameObject.CompareTag ("PC table") == true)
		{
			compOpenPanel.SetActive (false);
			collisionPC = false;
		}
	}
    void Awake()
    {
        if (PlayerPrefs.HasKey("PlayerX"))
        {
            PlayerPrefs.GetFloat("PlayerX");
        }
        if (PlayerPrefs.HasKey("PlayerZ"))
        {
            PlayerPrefs.GetFloat("PlayerZ");
        }
        if (PlayerPrefs.HasKey("PlayerY"))
        {
            PlayerPrefs.GetFloat("PlayerY");
        }
        player.transform.position.x = playerPosX;
        player.transform.position.y = playerPosY;
        player.transform.position.z = playerPosZ;
    }
}