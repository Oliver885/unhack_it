﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hack : MonoBehaviour {
    public InputField inputFieldHack;
    public string inputFieldText;

    // Use this for initialization
    void Start() {

    }
    // Update is called once per frame
    void Update()
    {
        if (inputFieldHack.text == "test")
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                inputFieldText = inputFieldHack.text + "test succes";
                inputFieldHack.text = inputFieldText;
                StartCoroutine(ClearingInputField());
            }
        }
    }
    public IEnumerator ClearingInputField()
    {
        yield return new WaitForSeconds(3.0f);
        inputFieldHack.text = "";
    }
}
